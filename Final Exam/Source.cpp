#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "Queue.h"
using namespace std;

void clearConsole() {
	system("pause");
	system("cls");
}
void main() {
	int count = 0;
	int inputSize;
	int inputNumber;
	int userUIInput;
	
	cout << "Input size  for element sets" << endl;
	cin >> inputSize;

	UnorderedArray<int> stack(inputSize);
	Queue<int> queue(inputSize);
	system("cls");

	while (true)
	{
		cout << "What would you want to do? " << endl; // USER CHOICES
		cout << "[1] - Push Elements" << endl;
		cout << "[2] - Pop Elements" << endl;
		cout << "[3] - Print everything and empty set" << endl;
		cin >> userUIInput;

		
		if (userUIInput == 1)
		{
			cout << "Enter a number: ";
			cin >> inputNumber;

			stack.push(inputNumber);
			queue.push(inputNumber);

			cout << "Top Element of Queue: "; queue.top(inputNumber);cout << endl;
			cout << "Top Element of Stack: "; stack.top(inputNumber); cout << endl;
			
		}
		else if (userUIInput == 2)
		{
			cout << "Popped the front elements!" << endl;
			stack.pop();
			queue.remove(0);
		
			cout << "Top Element of Queue: "; queue.top(inputNumber); cout << endl;
			cout << "Top Element of Stack: "; stack.top(inputNumber); cout << endl;
		}
		else if (userUIInput == 3)
		{
			cout << "Printing Queue..." << endl;
			for (int i = 0; i < queue.getSize(); i++) {
				cout << queue[i] << " " << endl;
			}

			cout << "Printing Stack..." << endl;
			for (int k = stack.getSize(); k > 0; k--) {
				cout << stack[k] << " " << endl;
			}
			/*
			UnorderedArray<int> stack(inputSize);
			Queue<int> queue(inputSize);*/
		}
		clearConsole();
	}
	system("pause");
}