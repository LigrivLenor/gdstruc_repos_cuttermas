#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void clearConsole() {
	system("pause");
	system("cls");
}
void main(){
	srand(static_cast<unsigned int>(time(NULL)));
	int input;
	int count = 0;
	int size;
	cout << "Enter size for dynamic array: ";
	cin >> size;
	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);
	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}
	system("cls");

	while (true)
	{
		cout << "Unordered contents: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << " ";
		cout << "\nOrdered contents:   ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << " ";
		cout << endl << endl;

		cout << "What would you like to do? " << endl; // USER CHOICES
		cout << "[1]- Remove element at index" << endl;
		cout << "[2]- Search for element" << endl;
		cout << "[3]- Expand and generate random values" << endl;
		cin >> input;

		if (input <= 1) // DELETING INDEX
		{
			cout << endl << endl;
			cout << "Input an index to remove: ";
			cin >> input; cout << endl;
			unordered.remove(input);
			ordered.remove(input);
			cout << "Element removed: " << input << endl;

			cout << "Unordered contents: "; //DISPLAYING Order of Elements after "Deleting"
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << " ";
			cout << "\nOrdered contents:   ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << " "; cout << endl;
		}
		else if (input == 2) //LINEAR and BINARY SEARCH
		{
			cout << endl;
			cout << "Input element to search: ";
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";        ////DISPLAYING Order of Elements after "Searching"
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;
		}
		else if (input == 3) // Expand and generate random values
		{
			cout << endl;
			cout << "Input size of expansion: ";
			cin >> input; cout << endl;
			for (int i = 0; i < input; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
			cout << "Arrays have been expanded:" << endl;
			cout << "Unordered contents: ";        ////DISPLAYING Order of Elements after "Expanding"
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << " ";
			cout << "\nOrdered contents:   ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << " "; cout << endl;
		}
		clearConsole();
	}
	system("pause");
}