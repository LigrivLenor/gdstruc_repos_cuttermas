#pragma once
#include <assert.h>
#include<iostream>
#include<string>
using namespace std;

template<class T>
class OrderedArray
{
public:
	OrderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~OrderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
			expand();

		mArray[mNumElements] = value;
		for (int i = 0; i < mNumElements; i++)
		{
			for (int j = 0; j < mNumElements; j++)
			{
				if (mArray[j] > mArray[i+1])
				{
					int temp = mArray[i+1];
					mArray[i+1] = mArray[j];
					mArray[j] = temp;
				}
			}
		}
		mNumElements++;
	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL && index <= mMaxSize);

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual T & operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}

	virtual int binarySearch(T val)
	{
		int l = 0;
		int r = mNumElements - 1;
		int count = 0;

		while (l <= r) {
			int mid =(l + r) / 2;

			if (mArray[mid] == val)
			{
				cout << "Binary Search took " << count << " comparisons" << endl;
				return mid;
			}
			else if (mArray[mid] > val) {
				r = mid - 1;
			}
			else
			{
				l = mid + 1;
			}
			count++;
		}
		return -1;
	}
private:
	T * mArray;
	int mGrowSize;
	int mMaxSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T * temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}

};