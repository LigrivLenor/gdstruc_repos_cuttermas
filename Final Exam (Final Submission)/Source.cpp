#include <iostream>
#include "UnorderedArray.h"
#include "Queue.h"
using namespace std;

void clearConsole() {
	system("pause");
	system("cls");
}
void main() {
	int inputSize;
	int inputNumber;
	int userUIInput;
	
	cout << "Input size  for element sets" << endl;
	cin >> inputSize;

	UnorderedArray<int> stack(inputSize);
	Queue<int> queue(inputSize);
	
	system("cls");

	while (true)
	{
		cout << "What would you want to do? " << endl; // USER CHOICES
		cout << "[1] - Push Elements" << endl;
		cout << "[2] - Pop Elements" << endl;
		cout << "[3] - Print everything and empty set" << endl << endl; // Press 3 times to completely empty set...
		cout << "Your Input: "; cin >> userUIInput;

		if (userUIInput <= 1)
		{
			cout << endl << endl;
			cout << "Enter a number: ";
			cin >> inputNumber;

			stack.push(inputNumber);
			queue.push(inputNumber);

			cout << "Top Element of Queue: "; queue.top(inputNumber);cout << endl;
			cout << "Top Element of Stack: "; stack.top(inputNumber);cout << endl;
			
		}
		else if (userUIInput == 2)
		{
			if (stack.getSize() && queue.getSize() != 0)
			{
				cout << endl << endl;
				cout << "You have popped the front elements." << endl;
				stack.pop();
				queue.remove(0);
		
				cout << "Top Element of Queue: "; queue.top(inputNumber); cout << endl;
				cout << "Top Element of Stack: "; stack.top(inputNumber); cout << endl;
			}
			else {
				cout << "You have popped the front elements." << endl;
			}
			
		}
		else if (userUIInput >= 3)
		{
			cout << "Queue elements: " << endl;
			for (int i = 0; i <= queue.getSize() - 1; i++) {
				cout << queue[i] << " " << endl;
			}

			cout << "Stack elements: " << endl;
			for (int s = stack.getSize() - 1; s >= 0; s--) {
				cout << stack[s] << " " << endl;
			}

			if (queue.getSize() && stack.getSize() != 0)
			{
				for (int p = 0; p <= queue.getSize() && stack.getSize(); p++)
				{
					stack.pop();
					queue.remove(0);
				}
			}
			else {
				cout << "Both Queue and Stack are empty." << endl;
			}
		}
		clearConsole();
	}
	system("pause");
}